#!/usr/local/bin/ruby
# 2021 Alain De Vos <devosalain@ymail.com>
['gtk3','open3','sqlite3'].each {|file| require file }

class MyLLabel < Gtk::Label
	def initialize(text,x,y)
	    super(text)
        set_xalign(0.01)
        set_size_request(x,y)
        set_opacity(0.9)
	end
end

class MyRLabel < Gtk::Label
	def initialize(text,x,y)
	    super(text)
        set_xalign(0.9)
        set_size_request(x,y)
        set_opacity(0.9)
	end
end

class MyWindow < Gtk::Window
	def initialize
		super
		set_title "My Window"
		set_border_width 10
		set_size_request 800,400
		set_default_size 800,400

		vbox=Gtk::Box.new(:vertical, 6)
		
			@entry = Gtk::Entry.new
			vbox.pack_start(@entry)
			@entry.text = "find /home/x/Src/test -print"

			button1 = Gtk::Button.new(label: 'Createdb')
			button1.signal_connect('clicked') { button1_on_click_me_clicked }
			vbox.pack_start(button1)
		
			button2 = Gtk::Button.new(label: 'Listdb')
			button2.signal_connect('clicked') { button2_on_click_me_clicked }
			vbox.pack_start(button2)

			scroll = Gtk::ScrolledWindow.new()
		
			inner = Gtk::Grid.new()
			scroll.add(inner)
			vbox.pack_start(scroll,1,1,1)
			
			@row=20
			@column=3
			@buttongrid = Array.new(@row) { Array.new(@column) }
			@labelgrid =  Array.new(@row) { Array.new(@column) }
			@privdata  =  Array.new(@row)
			@h=20
			
			columnsize = Array[400,150,200]
			for y in 0..@row-1 do
				for x in 0..@column-1 do
					@labelgrid[y][x] =MyLLabel.new("XXXXX",columnsize[x],@h)
					@buttongrid[y][x]=Gtk::Button.new(label: y.to_s)
					@buttongrid[y][x].set_opacity(0.1)
 					@buttongrid[y][x].signal_connect('clicked'){|b| clicked(b)
						}
				end
			end
			for y in 0..@row-1 do
				for x in 0..@column-1 do
					inner.attach(@labelgrid[y][x],x,y,1,1)
					inner.attach(@buttongrid[y][x],x,y,1,1)
				end
			end
			
		add(vbox)
	end
	
	def clicked(b)
		y=b.label.to_i
		puts y
		t=@labelgrid[y][0].text.chomp.reverse.chomp.reverse
		puts t
        cmd="geany "+t
        system(cmd)
	end


	def create_db
		@db = SQLite3::Database.new ":memory:"
		puts @db.get_first_value 'SELECT SQLITE_VERSION()'
	end

	def create_table
		@db.execute "CREATE TABLE IF NOT EXISTS file(name TEXT,size TEXT, time TEXT)"
	end 

    def do_aline(astring)
		begin
			afile=File.stat(astring)
			asize=afile.size.to_s.rjust(12, "0")
			atime=afile.ctime.to_s
		    puts astring,asize,atime
		    todo="INSERT INTO file(name , size , time) VALUES ('"+astring+"' , '"+asize+"' , '"+atime+"') "
		    puts todo
			@db.execute todo 
		    #rescue
			#softlinks to nothing
		end
    end

	def button1_on_click_me_clicked
		puts '"Click me" button was clicked'
		Open3.popen2(@entry.text, :chdir=>"/") do |stdin, stdout, wait_thr|
				astring=stdout.read.chomp
				asplit = astring.force_encoding('iso-8859-1').split("\n")
				asplit.each do | aline |
					#puts aline
					do_aline(aline)
					end
		end
    end

	def button2_on_click_me_clicked
		puts "Button 2 clicked"
		r=-1
		@db.execute( "select * from file" ) do |row|
			r=r+1
			p row
			for x in 0..@column-1 do
			    puts "****"+r.to_s+"****"+x.to_s+"****"+row[x]+"****"
				@labelgrid[r][x].label=row[x]
			end
		end
	end
end

app = MyWindow.new
app.create_db
app.create_table
app.signal_connect('delete-event') { Gtk.main_quit }
app.signal_connect('destroy')      { Gtk.main_quit }
app.show_all
Gtk.main
