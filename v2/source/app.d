import gtk.Button;
import gtk.CellRendererText;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.ListStore;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Statusbar;
import gtk.TreeIter;
import gtk.TreeModelIF;
import gtk.TreeIter;
import gtk.TreeSelection;
import gtk.TreePath;
import gtk.TreeView;
import gtk.TreeViewColumn;
import gtk.Widget;
import gdk.Event: Event;
import std.algorithm: sort;
import std.array: assocArray;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.stdio;
import std.file: getTimes, getSize, dirEntries, SpanMode,DirIterator;
import std.format : format;
import std.typecons : Nullable;
import std.path: extension;
import std.process: spawnShell;
import std.range: iota, retro, drop, zip;
import std.stdio: writeln;
import std.string: chop, strip,tr;
import std.regex: regex,matchAll;
import core.thread: Thread;
import glib.Idle: Idle;
import d2sqlite3;

class FillDbThread : Thread {
	string mydir;
	MyWindow mywindow;
	Database conn;
	string tablename;
	
    this(MyWindow amywindow,string amydir,Database aconn,string atablename) {
		mydir=amydir;
		mywindow=amywindow;
		conn=aconn;
		tablename=atablename;
        super(&run);
    }

private:

	void insert_db(string name,string modification,string access,string size,string myextension, ref int entries){
		try {
			string sql=format("INSERT INTO %s VALUES (:name, :modification, :access, :size, :myextension);",tablename);
			Statement statement = conn.prepare(sql);
			statement.bindAll(name,modification,access,size,myextension);
			statement.execute();
			entries=entries+1;
			}
		catch (Exception e){
			writeln("MYDBERROR :",name);
			}
	}

    void run(){
		DirIterator dFiles = dirEntries(mydir, SpanMode.depth);
		int entries=0;
		foreach (d; dFiles){
			if (! d.isDir()){
				if (d.isFile()) {
					SysTime fileAccessTime, fileModificationTime;
					string name=d;
					getTimes(name, fileAccessTime, fileModificationTime);
					string modification=fileModificationTime.toISOExtString()[0..16];
					            string access=fileAccessTime.toISOExtString()[0..16];
					string size=to!string(name.getSize());
					string myextension=extension(name).drop(1);
					insert_db(name,modification,access,size,myextension,entries);
					string s=format("Filling records:%s",entries);
					if (entries % 500 ==0) new Idle(delegate bool(){
							mywindow.statusbar.push(mywindow.contextID,s); 
							return false;
					});
				}
			}
		}
		string s=format("Filling database done , # records:%s",entries);
		new Idle(delegate bool(){
			mywindow.sensitive=true;
		    mywindow.statusbar.push(mywindow.contextID,s); 
		    mywindow.showbutton.setSensitive(mywindow.sensitive);
		    mywindow.fillbutton.setSensitive(mywindow.sensitive);
			return false;
        });
    }
}

class MyWindow : MainWindow {

    immutable int NAME=0;
    immutable int MODIFICATION=1;
    immutable int ACCESS=2;
    immutable int SIZE=3;
    immutable int EXTENSION=4;
    immutable int number_of_columns=5;
    immutable int HEIGHTSIZE=20;
	immutable int[number_of_columns] columnsize=[600,125,125,125,50];
	immutable float[number_of_columns] myalign=[0.0,1.0,1.0,1.0,1.0];
	immutable string[number_of_columns] columns=["Name","Modification","Access","Size","Extension"];
	bool sensitive=true;
    bool sort_order=false;
	int maxrows;
	uint contextID;
	string tablename;
    string[string] app_dict;
	Database conn;
	Button showbutton;
	Button fillbutton;
	Box vbox;
	Entry entrydir;
	Entry entryfilter;
	Statusbar statusbar;
	ListStore store;
	TreeView tv;

   	void quit(){
		conn.close();
		Main.quit();
	}
	
 	void show_handle(){
		store.clear();
		ResultRange answer =select_db();
		int num_stored_rows=0;
		foreach(Row arow; answer){
			string name=arow[NAME].as!string;
			string regfiltertext="\\b"~entryfilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.strip("_").matchAll(reg);
			int t=0;
			foreach (c; result) t+=1;
			if (t>0){
				string modification=arow[MODIFICATION].as!string;
				string access=arow[ACCESS].as!string;
				long size=arow[SIZE].as!long;
				string extension=arow[EXTENSION].as!string;
				store_addrow(name,modification,access,size,extension);
				num_stored_rows=num_stored_rows+1;
				if( num_stored_rows == maxrows )
					break;
			}
		}
		string s=format("Show records :%s",num_stored_rows);
		statusbar.push(contextID,s); 
	}	
		
	void filldb_handle(){
		statusbar.push(contextID, "Ask filling database");
		statusbar.queueDraw();
		vbox.queueDraw();
		if(sensitive==true){
			statusbar.push(contextID, "Start filling database");
			cleantable();
			sensitive=false;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			string mydir=entrydir.getText();
			Thread mythread = new FillDbThread(this,mydir,conn,tablename);
			mythread.start();
		}
	}

	void runmyfile(TreeSelection mysel){
		TreeIter it=get_row_selected(mysel);
		string name=store.getValueString(it,NAME);
		string extension=store.getValueString(it,EXTENSION);
		writeln("name:",name);
		writeln("extension:",extension);
		string exe;
		string* res;
		res= (extension in app_dict);
			if (res !is null){
				exe=*res;
				writeln("exe:",exe);
				string commandtorun=format("%s \"%s\"",*res,name);
				writeln(commandtorun);
				statusbar.push(contextID,commandtorun);
				spawnShell(commandtorun);
					}
			else
				statusbar.push(contextID,"Extension not found");
		};


//--DATABASE------------------------------------------------------------
	ResultRange select_db(){
		string sql=format(" SELECT * from %s ;",tablename);
		return(conn.execute(sql));
	};

	void cleantable(){
		string sql=format(" DELETE FROM %s ;",tablename);
		conn.run(sql);
	}

	void create_dbtable(string tablename){
		try {
			string sql=format("CREATE TABLE IF NOT EXISTS %s(name text, modification text,access text, size text, extension text)",tablename);
			conn.run(sql);
		}
		catch (Exception e){
			writeln("DBERROR CREATE TABLE ");
		}
	}		

//---WINDOW-------------------------------------------------------------
	void store_addrow(string name, string modification, string access,long size, string myextension){
		TreeIter it=store.createIter();
		store.setValue(it,NAME,name);
		store.setValue(it,MODIFICATION,modification);
		store.setValue(it,ACCESS,access);
		store.setValue(it,SIZE,size);
		store.setValue(it,EXTENSION,myextension);
	}
	
	void view_addcolumn(string name,int colindex,int mysize,float analign){
		TreeViewColumn col=new TreeViewColumn();
		col.setTitle(name);
		col.setSortColumnId(colindex);
		tv.appendColumn(col);
		CellRendererText  r=new CellRendererText();
		col.packStart(r,0);
		col.addAttribute(r,"text",colindex);
		r.setAlignment(analign,1.0);
		r.setFixedSize(mysize,HEIGHTSIZE);
	}

	TreeIter store_get_row(ref ListStore store, int index){ 
			TreeIter it;
			store.getIterFirst(it);
			for(int t=0;t<index;t++){
				store.iterNext(it);
			}
			return it;
		}

	TreeIter get_row_selected(TreeSelection mysel){
		TreeModelIF tmi;
		TreePath [] tp=mysel.getSelectedRows(tmi);
		int [] indices=tp[0].getIndices();
		int index=indices[0];
		writeln(index);
		return(store_get_row(store,index));
	};

//---CONSTRUCTOR--------------------------------------------------------
	this(Database aconn,int amaxrows,string atablename,int sizex,int sizey,string[string] anapp_dict){
		conn=aconn;
		create_dbtable(atablename);
		maxrows=amaxrows;
		tablename=atablename;
		app_dict=anapp_dict;
		cleantable();
		super("My Locate D-lang App");
		setSizeRequest(sizex,sizey);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);
			Box hboxdir=new Box(Orientation.HORIZONTAL,5);
			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				entrydir=new Entry("/home/x/Src/test");
				hboxdir.packStart(entrydir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);
			fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
			vbox.packStart(fillbutton,0,0,0);
			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				entryfilter=new Entry("*$");
				hboxfilter.packStart(entryfilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);
			showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle();});
			showbutton.setSensitive(sensitive);
			vbox.packStart(showbutton,0,0,0);
			ScrolledWindow s=new ScrolledWindow();
				store=new ListStore([GType.STRING,GType.STRING,GType.STRING,GType.LONG,GType.STRING]);
				store_addrow("1","2","3",4,"5");
				store_addrow("11","12","13",15,"16");
				tv=new TreeView();
				for(int t=0;t<number_of_columns;t++){
					view_addcolumn(columns[t],t,columnsize[t],myalign[t]);
				}
				tv.setModel(store);
				TreeSelection tsel=tv.getSelection();
				tsel.addOnChanged(delegate void(TreeSelection mysel){runmyfile(mysel);});
				s.add(tv);
			vbox.packStart(s,1,1,1);
			statusbar=new Statusbar();
			uint contextID=statusbar.getContextId("My Description");
			statusbar.push(contextID, "--------");
			vbox.packStart(statusbar,0,0,0);
		add(vbox);
		showAll();
	}
}

int main(string[] args){
	string tablename=" files" ;
    string[string] app_dict=["py"  : "/usr/local/bin/featherpad",
							 "wmv" : "/usr/local/bin/smplayer",
							 "avi" : "/usr/local/bin/smplayer"
							];
	int sizex=1100;
	int sizey=600;
	int maxrows=1000;
	Main.init(args);
	Database conn=Database("./sqlite3.db");
	MyWindow window=new MyWindow(conn,maxrows,tablename,sizex,sizey,app_dict);
	Main.run();
	return 0;
}
