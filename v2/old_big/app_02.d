import std.stdio;
import std.conv;
import ddb.postgres;
import std.file: getTimes,getSize,dirEntries,SpanMode;
import std.datetime.systime : SysTime;
import std.path: extension;
import std.range: retro,drop;
import std.string: format;
import gtk.Main;
import gtk.MainWindow;
import gtk.Widget;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Button;

class MyWindow : MainWindow {
	
	Entry adir;
	Entry afilter;
	void quit(){
		Main.quit();
	}

	void file_do(string f) {
		writeln(f);
		string name=f;
		SysTime fileAccessTime, fileModificationTime;
		getTimes(f, fileAccessTime, fileModificationTime);
		string creation="";
		string modification=fileModificationTime.toISOExtString();
		string access=fileAccessTime.toISOExtString();
		string size=to!string(f.getSize());
		string myextension=extension(f).drop(1);
		writeln(f,":::",creation,":::",modification,":::",access,":::",size,":::",myextension);
	}
	
	void filldb_handle(){
		string mydir=adir.getText();
		writeln("FILLDB_HANDLE :::",mydir);
		// Iterate over all files in current directory and all its subdirectories
		auto dFiles = dirEntries(mydir, SpanMode.depth);
		foreach (d; dFiles){
			if (! d.isDir())
				if (d.isFile()) 
					{file_do(d.name);}
		}
	}
	
	this(){
		super("My Locate D-lang App");
		setSizeRequest(400,300);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		Box vbox=new Box(Orientation.VERTICAL,5);

		Box hboxdir=new Box(Orientation.HORIZONTAL,5);
		hboxdir.packStart((new Label("Enter Dir")),1,1,0);
		adir=new Entry("/home/x/Src");
		hboxdir.packStart(adir,1,1,0);
		vbox.packStart(hboxdir,1,1,2);

		Button fillbutton=new Button("Fill");
		fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
		vbox.packStart(fillbutton,1,1,2);
		
		Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
		hboxfilter.packStart((new Label("Enter Filter")),1,1,0);
		afilter=new Entry("Filter Entry");
		hboxfilter.packStart(afilter,1,1,0);
		vbox.packStart(hboxfilter,1,1,2);

        Button showbutton=new Button("Show");
		vbox.packStart(showbutton,1,1,2);
		
		Label tablelabel=new Label("Table");
		vbox.packStart(tablelabel,1,1,2);

		add(vbox);
		showAll();
	}
}

Connection opendatabase(string db,string user,string pass){
	//string url = format!"postgresql://localhost:5432/%s?user=%s,password=%s,ssl=true"(db, user, pass);
	// creating Connection
    //Connection conn = createConnection(url);
    auto conn = new PGConnection(["host" : "127.0.0.1","database" : db,"user" : user,"password" : pass]);
    scope(exit) conn.close;
	return(conn);
	}

int main(string[] args){
	Main.init(args);
	Connection conn=opendatabase("x_db","x","xxxxxxxx");
	MyWindow window=new MyWindow();
	Main.run();
	return 0;
}
