#!/usr/bin/env /usr/local/bin/ruby
#Copyright (C) 2016 Alain De Vos <devosalain@ymail.com>
['Qt','open3'].each {|file| require file }

class MyWidget < Qt::Widget
  slots 'zoekbuttonclicked()', 'casebuttonclicked()', 'myselection(int,int)'

  def mytodo(todo)
    puts todo
    system(todo)
  end
  
  def myselection(getaly,getalx)
    getal=@atable.item(getaly,0).text.to_i-1
    myfile = @mystrings[getal]
    myfile = myfile[0..-1]
    $binmap.each { |k, v|
                    if (myfile =~ v[1] ) then
                          todo = v[0] + ' ' + '"' + myfile + '"'
                          mytodo(todo)
                    end
             }
  end

  def casebuttonclicked
    @mycase = 1 - @mycase
    tekst= if  @mycase==0
               "currently case sensitive"
           else
               "currently case insensitive"
           end
    @casebutton.setText(tekst)
  end

  def fillrow(t,astring)
    @anitem = Qt::TableWidgetItem.new((t + 1).to_s.rjust(6,"0"))
    color=Qt::Color.new()
    color.setRgb(245,240,245)
    brush=Qt::Brush.new(color)
    @anitem.setBackground(brush)
    @anitem.setFlags(@anitem.flags() & ~Qt::ItemIsEditable)
    @atable.setItem(t, 0, @anitem)
    @anitem = Qt::TableWidgetItem.new(astring)
    @atable.setItem(t, 1, @anitem)
    begin
      afile=File.stat(astring)
      @anitem = Qt::TableWidgetItem.new(File.stat(astring).size.to_s.rjust(12, "0"))
      @atable.setItem(t, 2, @anitem)
      @anitem = Qt::TableWidgetItem.new(File.stat(astring).ctime.to_s)
      @atable.setItem(t, 3, @anitem)
    rescue
      #softlinks to nothing
    end
  end
  
  def zoekbuttonclicked
    s=""
    @alayoutv.removeWidget(@atable)
    addatable
    @atable.setSortingEnabled(false)
    zoek = if @mycase == 0
      $mylocate+"    "+@alinedit.text()
           else
      $mylocate+" -i "+@alinedit.text()
           end
    o, s = Open3.capture2(zoek)
    @mystrings = o.force_encoding('iso-8859-1').split("\n")
    if @mystrings.size < 20_000
      @atable.setRowCount(@mystrings.size + 1)
      t = 0
      @mystrings.each do |afile|
        if(t%1000==0) then
          @alabel.setText("Working:"+t.to_s)
          @alabel.repaint()
          $awidget.repaint()
        end
        fillrow(t,afile)
        t=t+1
      end
      @atable.setSortingEnabled(true)
      @atable.sortItems(1, Qt::AscendingOrder)
    else
      puts 'SIZE:' + @mystrings.size.to_s
    end
    @alabel.setText("Results:"+@mystrings.size.to_s)
  end

  def addatable
    @atable = Qt::TableWidget.new
    @atable.setColumnCount(4)
    @atable.setColumnWidth(1, 600)
    @atable.setHorizontalHeaderItem(0, Qt::TableWidgetItem.new('Number'))
    @atable.setHorizontalHeaderItem(1, Qt::TableWidgetItem.new('Filename'))
    @atable.setHorizontalHeaderItem(2, Qt::TableWidgetItem.new('Size'))
    @atable.setHorizontalHeaderItem(3, Qt::TableWidgetItem.new('Ctime'))
    connect(@atable, SIGNAL('cellClicked(int,int)'), self, SLOT('myselection(int,int)'))
    @alayoutv.addWidget(@atable)
  end
  
  def doit1
    @mycase = 0
    setWindowTitle('Locateme')
    resize(1000, 800)
    move(100, 100)
    @alayouth = Qt::HBoxLayout.new()
    @alayoutv = Qt::VBoxLayout.new()
    @alinedit = Qt::LineEdit.new
    @alayoutv.addWidget(@alinedit)
    @abutton = Qt::PushButton.new('zoek')
    connect(@abutton, SIGNAL('clicked()'), self, SLOT('zoekbuttonclicked()'))
    @alayouth.addWidget(@abutton)
    @casebutton = Qt::PushButton.new('case')
    connect(@casebutton, SIGNAL('clicked()'), self, SLOT('casebuttonclicked()'))
    @alayouth.addWidget(@casebutton)
    @alayoutv.addItem(@alayouth)
    @alabel = Qt::Label.new('--------------')
    @alayoutv.addWidget(@alabel)
    addatable
    setLayout(@alayoutv)
    show
  end
end

def maindoit
	myapp = Qt::Application.new(ARGV)
	$awidget = MyWidget.new
	$awidget.doit1
	myapp.exec
end

$mylocate='/usr/bin/locate'
$binmap={ "video"=>['/usr/local/bin/smplayer',/\.(?:mpg|mpeg|vob|avi|asf|wma|wmv|rm|qt|mov|mp4|ogg|ogm|mkv|vivo|fli|film|roq|vcd|svcd|dvd|divx)$/i],
       "document"=>[   '/usr/local/bin/atril',/\.(?:ps|eps|djvu|dvi|xps|pdf)$/i],
          "audio"=>[  '/usr/local/bin/mpg123',/\.(?:mp3)$/i]
        }
maindoit
