void main(){

	import std.stdio: write,writef,writeln,stdout;
	import std.range : iota,retro;
	import core.thread: Thread,seconds;

	write("Greeting in   ");
	foreach(num ; iota(1,4).retro){
		writef("%s...",num);
		stdout.flush();
		Thread.sleep(1.seconds);
	}
	writeln();
	writeln("Hello World");
}
