
import dpq2;
import gtk.Button;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Widget;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.file: getTimes,getSize,dirEntries,SpanMode;
import std.format : format ;
import std.path: extension;
import std.range: retro,drop;
import std.stdio: writeln;
import std.string: chop,strip;
import std.range: iota;
import std.regex;


class MyLLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.1);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyRLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.9);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyWindow : MainWindow {

	Connection conn;
	bool sensitive;
	int maxrows;
	Button showbutton;
	Button [800][6] mybut;
	Label [800][6] mylabel;
	string tablepredata;
	Box vbox;
	Grid inner;
	Entry adir;
	Entry afilter;
	int num_table_rows;
    bool namesortbool;
    
	void quit(){
		Main.quit();
	}

 	void sortme_alpha_min(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() < mylabel[c][i+2].getText()) {
					foreach (k ; 0..6){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

	void sortme_alpha_plus(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() > mylabel[c][i+2].getText()) {
					foreach (k ; 0..6){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

    void name_sort(){
		if (namesortbool==true)
			namesortbool=false;
		else
			namesortbool=true;
			
		if (namesortbool==true){
			writeln("SORTPLUS");
			sortme_alpha_plus(0);
		}
		else	{
			writeln("SORTMIN");
			sortme_alpha_min(0);
		}
	}

	void myaddgrid(){
		writeln("MYGRIDSTART");
		int[] columnsize=[400,1,150,150,100,50];
		string[] columns=["Name","C","Modification","Access","Size","Extension"];
		mylabel[0][0] = new MyRLabel("XXXXXXXXXXXXX",400,20);
		mylabel[0][0].setOpacity(0.5);
		inner.attach(mylabel[0][0], 0, 0,1,1);	
		foreach( x ; 0..6){
		    string labelText = format("Sort %s", columns[x]);
		    mybut[x][0] = new Button(labelText);
		    mybut[x][0].setSizeRequest(columnsize[x],20);
		    inner.attach(mybut[x][0], x, 0,1,1);	
			}
		mybut[0][0].addOnClicked(delegate void(Button b){name_sort();});
		

		writeln("MYADDGRID2:",maxrows);
		foreach( y ; 1..maxrows){
			foreach( x ; 0..6){
				if(x>3)
					mylabel[x][y] = new MyRLabel("",columnsize[x],20);
				else
					mylabel[x][y] = new MyLLabel("",columnsize[x],20);
				inner.attach(mylabel[x][y],x,y,1,1);	
				mybut[x][y] = new Button("");
				mybut[x][y].setSizeRequest(columnsize[x],20);
				inner.attach(mybut[x][y],x,y,1,1);	
			}
		}
		writeln("MYGRIDDONE");
 	}
 	
	void show_handle(Connection conn){
		inner.add(new Label("test"));
		inner.queueDraw();
		writeln("SHOW DATA BEGIN");
		string[] columns=["Name","C","Modification","Access","Size","Extension"];
		string sql=format(" SELECT * from %s ;",tablepredata);
		auto answer = conn.exec(sql);
		writeln(answer[0][0].as!PGtext);
		writeln(answer.length);
		num_table_rows=-1;
		foreach(rownumber; answer.length.iota){
			auto arow=answer[rownumber];
			string name=arow[0].as!PGtext;
			string regfiltertext="\\b"~afilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.strip("_").matchAll(reg);
			int t=0;
			foreach (c; result)
				t=t+1;
			if (t>0){
				num_table_rows=num_table_rows+1;
				foreach(int xpos; 0..6){
					mylabel[xpos][num_table_rows+1].setLabel(arow[xpos].as!PGtext);
				}
			if(num_table_rows>maxrows-1)
				break;
			}
		}
		writeln("SHOW DATA DONE");		
	}	
		
	void file_do(string f) {
		//writeln(f);
		ulong l;
		char c;
		string name=f;
		SysTime fileAccessTime, fileModificationTime;
		getTimes(f, fileAccessTime, fileModificationTime);
		string creation="XXX";
		string modification=fileModificationTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		l=modification.length;
		if(modification[l-1]=='.')
			modification=modification.chop();
		string access=fileAccessTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		l=access.length;
		if(access[l-1]=='.')
			access=access.chop();
		string size=to!string(f.getSize());
		string myextension=extension(f).drop(1);
   		//writeln(f,":::",creation,":::",modification,":::",access,":::",size,":::",myextension);
		string sql=format("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s');",tablepredata,name,creation,modification,access,size,myextension);
		//writeln(sql);
		auto answer = conn.exec(sql);
		
	}
	
	
	void cleantable(){
		auto answer = conn.exec(format(" TRUNCATE %s ;",tablepredata));
	}
	
	
	void filldb_handle(){
		sensitive=false;
		showbutton.setSensitive(sensitive);
		string mydir=adir.getText();
		writeln("FILLDB_HANDLE :::",mydir);
		cleantable();

		// Iterate over all files in current directory and all its subdirectories
		auto dFiles = dirEntries(mydir, SpanMode.depth);
		foreach (d; dFiles){
			if (! d.isDir())
				if (d.isFile()) 
					{file_do(d.name);}
		}
		sensitive=true;
		showbutton.setSensitive(sensitive);
		writeln("DONE WRITING DB");
	}
	
	this(Connection myconn,int mymaxrows,string mytablepredata){
		conn=myconn;
		maxrows=mymaxrows;
		tablepredata=mytablepredata;
		sensitive=false;
		cleantable();
		namesortbool=false;
		super("My Locate D-lang App");
		setSizeRequest(1000,600);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);

			Box hboxdir=new Box(Orientation.HORIZONTAL,5);
			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				adir=new Entry("/home/x/Src");
				hboxdir.packStart(adir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);

			Button fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
			vbox.packStart(fillbutton,0,0,0);

			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				afilter=new Entry("*conf$");
				hboxfilter.packStart(afilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);

			showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle(conn);});
			showbutton.setSensitive(sensitive);
			vbox.packStart(showbutton,0,0,0);

			ScrolledWindow s=new ScrolledWindow();
				inner=new Grid();
 				myaddgrid();
				s.add(inner);
			vbox.packStart(s,1,1,1);

		add(vbox);
		showAll();
	}
}

Connection opendatabase(string db,string user,string pass){
    string url=format("hostaddr='127.0.0.1' port='5432' dbname='%s' user='%s' password='%s'",db,user,pass);
    writeln(url);
	Connection conn = new Connection(url);
	auto answer = conn.exec("SELECT version()");
	writeln(answer[0][0].as!PGtext);
	return conn;
	}

int main(string[] args){
	Main.init(args);
	Connection conn=opendatabase("x_db","x","xxxxxxxx");
	MyWindow window=new MyWindow(conn,400," files ");
	Main.run();
	return 0;
}
