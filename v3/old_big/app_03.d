import dpq2;
import gtk.Button;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Widget;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.file: getTimes,getSize,dirEntries,SpanMode;
import std.format : format ;
import std.path: extension;
import std.range: retro,drop;
import std.stdio: writeln;
import std.range: iota;
import std.regex;

class MyWindow : MainWindow {
	
	string tablepredata;
	Connection conn;
	Box vbox;
	Grid inner;
	Button [30][6] mybut;
	
	Entry adir;
	Entry afilter;
	void quit(){
		Main.quit();
	}
	
	void show_handle(Connection conn){
		inner.add(new Label("test"));
		inner.queueDraw();
		writeln("SHOW DATA BEGIN");
		string[] columns=["Name","Creation","Modification","Access","Size","Extension"];
		string sql=format(" SELECT * from %s ;",tablepredata);
		auto answer = conn.exec(sql);
		writeln(answer[0][0].as!PGtext);
		writeln(answer.length);
		int y=-1;
		foreach(rownumber; answer.length.iota){
			auto arow=answer[rownumber];
			string name=arow[0].as!PGtext;
			string regfiltertext="\\b"~afilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.matchAll(reg);
			int t=0;
			foreach (c; result)
				t=t+1;
			if ((t>0)&&(t<30)){
				writeln(t,":::",name);
				//inner.add(new Label("name"));
				y=y+1;
				foreach(int xpos; 0..6){
					mybut[xpos][y].setLabel(arow[xpos].as!PGtext);

				}
			}
		}
		writeln("SHOW DATA DONE");		
	}	
		
	void file_do(string f) {
		//writeln(f);
		string name=f;
		SysTime fileAccessTime, fileModificationTime;
		getTimes(f, fileAccessTime, fileModificationTime);
		string creation="";
		string modification=fileModificationTime.toISOExtString();
		string access=fileAccessTime.toISOExtString();
		string size=to!string(f.getSize());
		string myextension=extension(f).drop(1);
		//writeln(f,":::",creation,":::",modification,":::",access,":::",size,":::",myextension);
		string sql=format("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s');",tablepredata,name,creation,modification,access,size,myextension);
		//writeln(sql);
		auto answer = conn.exec(sql);
		
	}
	void filldb_handle(Connection conn){
		string mydir=adir.getText();
		writeln("FILLDB_HANDLE :::",mydir);
		auto answer = conn.exec(" TRUNCATE files;");
		

		// Iterate over all files in current directory and all its subdirectories
		auto dFiles = dirEntries(mydir, SpanMode.depth);
		foreach (d; dFiles){
			if (! d.isDir())
				if (d.isFile()) 
					{file_do(d.name);}
		}
		writeln("DONE WRITING DB");
	}
	
	this(Connection aconn){
		conn=aconn;
		tablepredata="files";
		super("My Locate D-lang App");
		setSizeRequest(1400,600);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		
		vbox=new Box(Orientation.VERTICAL,5);


			Box hboxdir=new Box(Orientation.HORIZONTAL,5);

			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				adir=new Entry("/home/x/Src");
				hboxdir.packStart(adir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);

			Button fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle(conn);});
			vbox.packStart(fillbutton,0,0,0);
		
			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				afilter=new Entry("*conf$");
				hboxfilter.packStart(afilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);

			Button showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle(conn);});
			vbox.packStart(showbutton,0,0,0);
			
				ScrolledWindow s=new ScrolledWindow();
					inner=new Grid();

					myaddgrid();

				s.add(inner);


			vbox.packStart(s,1,1,1);
		
		add(vbox);
		showAll();
	}
	
	void myaddgrid(){
		int[] columnsize=[400,200,200,200,200,50,50];
		foreach( y ; 0..30){
			foreach( x ; 0..6){
		    string labelText = format("cell %d, %d", x, y);
		    mybut[x][y] = new Button(labelText);
		    mybut[x][y].setSizeRequest(columnsize[x],20);
		    inner.attach(mybut[x][y], x, y,1,1);	
			}
		}
	}
	
}

Connection opendatabase(string db,string user,string pass){
    string url=format("hostaddr='127.0.0.1' port='5432' dbname='%s' user='%s' password='%s'",db,user,pass);
    writeln(url);
	Connection conn = new Connection(url);
	auto answer = conn.exec("SELECT version()");
	writeln(answer[0][0].as!PGtext);
	return conn;
	}

int main(string[] args){
	Main.init(args);
	Connection conn=opendatabase("x_db","x","xxxxxxxx");
	MyWindow window=new MyWindow(conn);
	Main.run();
	return 0;
}
