
import dpq2;
import gtk.Button;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Widget;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.file: getTimes,getSize,dirEntries,SpanMode;
import std.format : format ;
import std.path: extension;
import std.range: retro,drop;
import std.stdio: writeln;
import std.string: chop,strip;
import std.range: iota;
import std.regex;


class MyLLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.1);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyRLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.9);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyWindow : MainWindow {

	Connection conn;
	Button [800][6] mybut;
	Label [800][6] mylabel;
    immutable int number_of_columns=6;
	immutable int[] columnsize=[400,1,150,150,100,50];
	immutable string[] columns=["Name","C","Modification","Access","Size","Extension"];
	bool sensitive=true;
    bool namesortbool=false;
	int maxrows;
	Button showbutton;
	Button fillbutton;
	string tablepredata;
	Box vbox;
	Grid inner;
	Entry adir;
	Entry afilter;
	int num_table_rows;
    
	void quit(){
		Main.quit();
	}

 	void sortme_alpha_min(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() < mylabel[c][i+2].getText()) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

	void sortme_alpha_plus(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() > mylabel[c][i+2].getText()) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

 	void sortme_num_min(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (to!int(mylabel[c][i+1].getText()) < to!int(mylabel[c][i+2].getText())) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

	void sortme_num_plus(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (to!int(mylabel[c][i+1].getText()) > to!int(mylabel[c][i+2].getText())) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}


    void name_sort(){
		if (namesortbool==true) namesortbool=false; else namesortbool=true;
		if (namesortbool==true) sortme_alpha_plus(0); else sortme_alpha_min(0);
	}

    void size_sort(){
		if (namesortbool==true) namesortbool=false; else namesortbool=true;
		if (namesortbool==true) sortme_num_plus(4); else sortme_num_min(4);
	}

	void myaddgrid(){
		foreach( x ; 0..number_of_columns){
		    string labelText = format("Sort %s", columns[x]);
		    mybut[x][0] = new Button(labelText);
		    mybut[x][0].setSizeRequest(columnsize[x],20);
		    inner.attach(mybut[x][0], x, 0,1,1);	
			}
		mybut[0][0].addOnClicked(delegate void(Button b){name_sort();});
		mybut[4][0].addOnClicked(delegate void(Button b){size_sort();});
		
		foreach( y ; 1..maxrows){
			foreach( x ; 0..number_of_columns){
				if(x>3)
					mylabel[x][y] = new MyRLabel("",columnsize[x],20);
				else
					mylabel[x][y] = new MyLLabel("",columnsize[x],20);
				inner.attach(mylabel[x][y],x,y,1,1);	
				mybut[x][y] = new Button("");
				mybut[x][y].setSizeRequest(columnsize[x],20);
				inner.attach(mybut[x][y],x,y,1,1);	
			}
		}
 	}
 	
	void show_handle(Connection conn){
		inner.add(new Label("test"));
		inner.queueDraw();
		string[] columns=["Name","C","Modification","Access","Size","Extension"];
		string sql=format(" SELECT * from %s ;",tablepredata);
		auto answer = conn.exec(sql);
		num_table_rows=-1;
		foreach(rownumber; answer.length.iota){
			auto arow=answer[rownumber];
			string name=arow[0].as!PGtext;
			string regfiltertext="\\b"~afilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.strip("_").matchAll(reg);
			int t=0;
			foreach (c; result)
				t=t+1;
			if (t>0){
				num_table_rows=num_table_rows+1;
				foreach(int xpos; 0..number_of_columns){
					mylabel[xpos][num_table_rows+1].setLabel(arow[xpos].as!PGtext);
				}
			if(num_table_rows>maxrows-1)
				break;
			}
		}
	}	
		
	void file_do(string f) {
		ulong l;
		char c;
		string name=f;
		SysTime fileAccessTime, fileModificationTime;
		getTimes(f, fileAccessTime, fileModificationTime);
		string creation="XXX";
		string modification=fileModificationTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		l=modification.length;
		if(modification[l-1]=='.')
			modification=modification.chop();
		string access=fileAccessTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		l=access.length;
		if(access[l-1]=='.')
			access=access.chop();
		string size=to!string(f.getSize());
		string myextension=extension(f).drop(1);
		string sql=format("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s');",tablepredata,name,creation,modification,access,size,myextension);
		auto answer = conn.exec(sql);
		
	}
	
	
	void cleantable(){
		auto answer = conn.exec(format(" TRUNCATE %s ;",tablepredata));
	}
	
	
	void filldb_handle(){
		writeln("FILLDB");
		if(sensitive==true){
			writeln("FILLDB*****");
			sensitive=false;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			string mydir=adir.getText();
			cleantable();

			auto dFiles = dirEntries(mydir, SpanMode.depth);
			foreach (d; dFiles){
				if (! d.isDir())
					if (d.isFile()) 
						{file_do(d.name);}
			}
			sensitive=true;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			writeln("FILLDB DONE");
		}
	}
	
	this(Connection myconn,int mymaxrows,string mytablepredata){
		conn=myconn;
		maxrows=mymaxrows;
		tablepredata=mytablepredata;
		cleantable();
		super("My Locate D-lang App");
		setSizeRequest(1000,600);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);

			Box hboxdir=new Box(Orientation.HORIZONTAL,5);
			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				adir=new Entry("/home/x/Src");
				hboxdir.packStart(adir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);

			fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
			vbox.packStart(fillbutton,0,0,0);

			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				afilter=new Entry("*conf$");
				hboxfilter.packStart(afilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);

			showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle(conn);});
			showbutton.setSensitive(sensitive);
			vbox.packStart(showbutton,0,0,0);

			ScrolledWindow s=new ScrolledWindow();
				inner=new Grid();
 				myaddgrid();
				s.add(inner);
			vbox.packStart(s,1,1,1);

		add(vbox);
		showAll();
	}
}

Connection opendatabase(string db,string user,string pass){
    string url=format("hostaddr='127.0.0.1' port='5432' dbname='%s' user='%s' password='%s'",db,user,pass);
	Connection conn = new Connection(url);
	auto answer = conn.exec("SELECT version()");
	return conn;
	}

int main(string[] args){
	Main.init(args);
	Connection conn=opendatabase("x_db","x","xxxxxxxx");
	MyWindow window=new MyWindow(conn,400," files ");
	Main.run();
	return 0;
}
