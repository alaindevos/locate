import gtk.Button;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Statusbar;
import gtk.Widget;
import gdk.Event: Event;
import std.algorithm: sort;
import std.array: assocArray;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.stdio;
import std.file: getTimes, getSize, dirEntries, SpanMode;
import std.format : format;
import std.typecons : Nullable;
import std.path: extension;
import std.process: spawnShell;
import std.range: iota, retro, drop, zip;
import std.stdio: writeln;
import std.string: chop, strip,tr;
import std.regex: regex,matchAll;
import core.thread: Thread;
import glib.Idle: Idle;
import d2sqlite3;

class MyLLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.1);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyRLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.9);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class DerivedThread : Thread {
	string mydir;
	MyWindow mywindow;
	Database conn;
	string db_table;
    this(MyWindow amywindow,string amydir,Database aconn,string adb_table) {
		mydir=amydir;
		mywindow=amywindow;
		conn=aconn;
		db_table=adb_table;
        //writeln("Thread INIT");
        super(&run);
    }

private:
    void run(){
		auto dFiles = dirEntries(mydir, SpanMode.depth);
		int entries=0;
		foreach (d; dFiles){
			if (! d.isDir()){
				if (d.isFile()) {
					entries=entries+1;
					SysTime fileAccessTime, fileModificationTime;
					string name=d;
					getTimes(name, fileAccessTime, fileModificationTime);
					string creation="XXX";
					string modification=fileModificationTime.toISOExtString().chop().chop().chop().chop().chop().chop();
					if(modification[modification.length-1]=='.') modification=modification.chop();
					string access=fileAccessTime.toISOExtString().chop().chop().chop().chop().chop().chop();
					if(access[access.length-1]=='.') access=access.chop();
					string size=to!string(name.getSize());
					string myextension=extension(name).drop(1);
					string name2=name;
					name2=name2.tr(`-`,"_");
					name2=name2.tr(`@`,"_");
					name2=name2.tr(`"`,"_");
					name2=name2.tr(`'`,"_");
					name2=name2.tr(`(`,"_");
					name2=name2.tr(`)`,"_");
					name2=name2.tr(`[`,"_");
					name2=name2.tr(`]`,"_");
					//writeln("Adding:",name2);
					try {
						string sql=format("INSERT INTO %s VALUES (:name, :creation, :modification, :access, :size, :myextension);",db_table);
						Statement statement = conn.prepare(sql);
						statement.bindAll(name2,creation,modification,access,size,myextension);
						statement.execute();
					}
					catch (Exception e){
						writeln("DBERROR :",name2,name);
					}
				}
			}
		}
		string s=format("Filling database done , Estimated # records:%s",entries);
		new Idle(delegate bool(){
			mywindow.sensitive=true;
		    mywindow.statusbar.push(mywindow.contextID,s); 
		    mywindow.showbutton.setSensitive(mywindow.sensitive);
		    mywindow.fillbutton.setSensitive(mywindow.sensitive);
			return false;
        });
    }
}

class MyWindow : MainWindow {



    string[string] app_dict;
    immutable int number_of_columns=6;
	immutable int[] columnsize=[400,1,150,150,100,50];
	immutable string[] columns=["Name","C","Modification","Access","Size","Extension"];
	bool sensitive=true;
    bool sort_order=false;
	//     ROWy COLx mybut[COLx][ROWy]=...
	Button [455][6] mybut;
	string [] mycol0;
	string [] mycol1;
	string [] mycol2;
	string [] mycol3;
	string [] mycol4;
	int    [] mycol4num;
	string [] mycol5;
	Label [455][6] mylabel;
	int   [455] privdatay;
	int   [6] privdatax;
	int maxrows;
	int num_table_rows;
	string db_table;
	Database conn;

	Button showbutton;
	Button fillbutton;
	Box vbox;
	Grid inner;
	Entry adir;
	Entry afilter;
	Statusbar statusbar;
	uint contextID;
    
   	void quit(){
		Main.quit();
	}
	
	//string s=format("Found x=%s , y=%s , file=%s",to!string(*x),to!string(*y),name);
	//statusbar.push(contextID,s);

	bool gridbuttonreleased(Event event,Widget widget){
		Button b=cast(Button)widget;
		statusbar.push(contextID,"gridbuttonreleased");
        int *x=cast(int*) b.getData("x");
        int *y=cast(int*) b.getData("y");
		string name=mylabel[0][*y].getText();
        string extension=mylabel[5][*y].getText();
		string exe;
		string* res;
		res= (extension in app_dict);
		if (res !is null){
				exe=*res;
				string commandtorun=format("%s %s",*res,name);
				statusbar.push(contextID,commandtorun);
				spawnShell(commandtorun);
		}
		else
			statusbar.push(contextID,"Extension not found");
   		return false;
	}

    void size_sort(int kol){
		mycol0 = new string[num_table_rows];
		mycol1 = new string[num_table_rows];
		mycol2 = new string[num_table_rows];
		mycol3 = new string[num_table_rows];
		mycol4 = new string[num_table_rows];
		mycol4num = new int[num_table_rows];
		mycol5 = new string[num_table_rows];
		if (sort_order==true) sort_order=false; else sort_order=true;
		foreach( y ; 1..num_table_rows+1){
			mycol0[y-1]=mylabel[0][y].getText();
			mycol1[y-1]=mylabel[1][y].getText();
			mycol2[y-1]=mylabel[2][y].getText();
			mycol3[y-1]=mylabel[3][y].getText();
			mycol4[y-1]=mylabel[4][y].getText();
			mycol4num[y-1]=to!int(mycol4[y-1]);
			mycol5[y-1]=mylabel[5][y].getText();
		}
		auto myzip=zip(mycol0,mycol1,mycol2,mycol3,mycol4,mycol5,mycol4num);
		if(sort_order==true){
			switch(kol){
				case 0:
					myzip.sort!"a[0] < b[0]";
					break;
				case 2:
					myzip.sort!"a[2] < b[2]";
					break;
				case 3:
					myzip.sort!"a[3] < b[3]";
					break;
				case 5:
					myzip.sort!"a[5] < b[5]";
					break;
				case 6:
					myzip.sort!"a[6] < b[6]";
					break;
				default:
					break;
			}
		}
		else {
			switch(kol){
				case 0:
					myzip.sort!"a[0] > b[0]";
					break;
				case 2:
					myzip.sort!"a[2] > b[2]";
					break;
				case 3:
					myzip.sort!"a[3] > b[3]";
					break;
				case 5:
					myzip.sort!"a[5] > b[5]";
					break;
				case 6:
					myzip.sort!"a[6] > b[6]";
					break;
				default:
					break;
			}
		}
		foreach( y ; 1..num_table_rows+1){
			mylabel[0][y].setText(mycol0[y-1]);
			mylabel[1][y].setText(mycol1[y-1]);
			mylabel[2][y].setText(mycol2[y-1]);
			mylabel[3][y].setText(mycol3[y-1]);
			mylabel[4][y].setText(mycol4[y-1]);
			mylabel[5][y].setText(mycol5[y-1]);
		}
	}

	void myaddgrid(){
		//Uses maxrows
		int h=20;
		foreach( x ; 0..number_of_columns){
		    string labelText = format("Sort %s", columns[x]);
		    mybut[x][0] = new Button(labelText);
		    mybut[x][0].setSizeRequest(columnsize[x],h);
		    inner.attach(mybut[x][0], x, 0,1,1);	
			}
					
		mybut[0][0].addOnClicked(delegate void(Button b){size_sort(0);});
		mybut[2][0].addOnClicked(delegate void(Button b){size_sort(2);});
		mybut[3][0].addOnClicked(delegate void(Button b){size_sort(3);});
		mybut[4][0].addOnClicked(delegate void(Button b){size_sort(6);});
		mybut[5][0].addOnClicked(delegate void(Button b){size_sort(5);});
		
		foreach( y ; 1..maxrows){
			foreach( x ; 0..number_of_columns){
				if(x>3)
					mylabel[x][y] = new MyRLabel("",columnsize[x],h);
				else
					mylabel[x][y] = new MyLLabel("",columnsize[x],h);
				inner.attach(mylabel[x][y],x,y,1,1);	
				mybut[x][y] = new Button("");
				mybut[x][y].setSizeRequest(columnsize[x],h);
				privdatax[x]=x;
				privdatay[y]=y;
				mybut[x][y].setData("x",&privdatax[x]);
				mybut[x][y].setData("y",&privdatay[y]);
				mybut[x][y].addOnButtonRelease(&gridbuttonreleased);
				inner.attach(mybut[x][y],x,y,1,1);	
			}
		}
 	}
 	
	void show_handle(Database conn){
		// Uses maxrows
		string[] columns=["Name","C","Modification","Access","Size","Extension"];
		inner.add(new Label("test"));
		inner.queueDraw();
		string sql=format(" SELECT * from %s ;",db_table);
		ResultRange answer = conn.execute(sql);
		num_table_rows=0;
		int rownumber = -1;
		foreach(arow; answer){
			rownumber=rownumber +1;
			string name=arow[0].as!string;
			string regfiltertext="\\b"~afilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.strip("_").matchAll(reg);
			int t=0;
			foreach (c; result)
				t=t+1;
			if (t>0){
				num_table_rows=num_table_rows+1;
				foreach(int x; 0..number_of_columns){
					mylabel[x][num_table_rows].setLabel(arow[x].as!string);
				}
			// !!!!!!!!!!!!
			if( num_table_rows + 2 > maxrows )
				break;
			}
		//not counting header
		string s=format("Show records :%s",num_table_rows);
		statusbar.push(contextID,s); 
		}
	}	
		
	
	void cleantable(){
		//string sql=format(" TRUNCATE %s ;",db_table);
		string sql=format(" DELETE FROM %s ;",db_table);
		writeln(sql);
		//Statement statement=conn.prepare(sql);
		conn.run(sql);
		
	}

	void filldb_sub(){
			sensitive=false;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			string mydir=adir.getText();
			cleantable();
			Thread mythread = new DerivedThread(this,mydir,conn,db_table);
			mythread.start();
	}
	
	void filldb_handle(){
		statusbar.push(contextID, "Ask filling database");
		statusbar.queueDraw();
		vbox.queueDraw();
		if(sensitive==true){
			statusbar.push(contextID, "Start filling database");
			filldb_sub();
		}
	}
	
void create_dbtable(string tablename){
	//string sql=" DROP table files";
	try {
		string sql=format("create table %s(name text, creation text, modification text,access text, size text, extension text)",tablename);
		writeln(sql);
		conn.run(sql);
		writeln("TABLE files CREATED");
	}
	catch (Exception e){
		writeln("DBERROR CREATE TABLE ");
	}
}		


	this(Database myconn,int mymaxrows,string tablename,int sizex,int sizey,string[string] myapp_dict){
		conn=myconn;
		create_dbtable(tablename);
		maxrows=mymaxrows;
		db_table=tablename;
		app_dict=myapp_dict;
		cleantable();
		super("My Locate D-lang App");
		setSizeRequest(sizex,sizey);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);

			Box hboxdir=new Box(Orientation.HORIZONTAL,5);
			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				adir=new Entry("/home/x/Src");
				hboxdir.packStart(adir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);

			fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
			vbox.packStart(fillbutton,0,0,0);

			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				afilter=new Entry("*conf$");
				hboxfilter.packStart(afilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);

			showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle(conn);});
			showbutton.setSensitive(sensitive);
			vbox.packStart(showbutton,0,0,0);

			ScrolledWindow s=new ScrolledWindow();
				inner=new Grid();
 				myaddgrid();
				s.add(inner);
			vbox.packStart(s,1,1,1);

			statusbar=new Statusbar();
			uint contextID=statusbar.getContextId("My Description");
			statusbar.push(contextID, "--------");
			vbox.packStart(statusbar,0,0,0);
		
		add(vbox);
		showAll();
	}
}

Database opendatabase(){
	writeln("Creating DB");
	Database conn = Database(":memory:");
	writeln("Database Created");
	return conn;
	}

int main(string[] args){
	string tablename=" files" ;
    string[string] app_dict=["conf" : "/usr/local/bin/leafpad"];

	int sizex=1000;
	int sizey=600;
	int maxrows=355;
	Main.init(args);
	Database conn=opendatabase();
	MyWindow window=new MyWindow(conn,maxrows,tablename,sizex,sizey,app_dict);
	Main.run();
	return 0;
}
