import dpq2: Connection,as,PGtext;
import gtk.Button;
import gtk.Box;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.Main;
import gtk.MainWindow;
import gtk.ScrolledWindow;
import gtk.Statusbar;
import gtk.Widget;
import gdk.Event;
import std.array : assocArray;
import std.conv: to;
import std.datetime.systime : SysTime;
import std.file: getTimes,getSize,dirEntries,SpanMode;
import std.format : format ;
import std.path: extension;
import std.process: spawnShell;
import std.range: retro,drop;
import std.stdio: writeln;
import std.string: chop,strip;
import std.range: iota;
import std.regex;

class MyLLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.1);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyRLabel : Label {
	this(string text,int x,int y){
		super(text);
		setXalign(0.9);
		setSizeRequest(x,y);
		setOpacity(0.9);
	}
}

class MyWindow : MainWindow {

    string[string] app_dict;
    immutable int number_of_columns=6;
	immutable int[] columnsize=[400,1,150,150,100,50];
	immutable string[] columns=["Name","C","Modification","Access","Size","Extension"];
	//     ROWy COLx mybut[COLx][ROWy]=...
	bool sensitive=true;
    bool sort_order=false;
	Button [455][6] mybut;
	Label [455][6] mylabel;
	int   [455] privdatay;
	int   [6] privdatax;
	int maxrows;
	int num_table_rows;
	string db_table;
	Connection conn;

	Button showbutton;
	Button fillbutton;
	Box vbox;
	Grid inner;
	Entry adir;
	Entry afilter;
	Statusbar statusbar;
	uint contextID;
    
   	void quit(){
		Main.quit();
	}
	
	//string s=format("Found x=%s , y=%s , file=%s",to!string(*x),to!string(*y),name);
	//statusbar.push(contextID,s);

	bool gridbuttonreleased(Event event,Widget widget){
		Button b=cast(Button)widget;
		statusbar.push(contextID,"gridbuttonreleased");
        int *x=cast(int*) b.getData("x");
        int *y=cast(int*) b.getData("y");
		string name=mylabel[0][*y].getText();
        string extension=mylabel[5][*y].getText();
		string exe;
		string* res;
		res= (extension in app_dict);
		if (res !is null){
				exe=*res;
				string commandtorun=format("%s %s",*res,name);
				statusbar.push(contextID,commandtorun);
				spawnShell(commandtorun);
		}
		else
			statusbar.push(contextID,"Extension not found");
   		return false;
	}
	
 	void sortme_alpha_min(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() < mylabel[c][i+2].getText()) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

	void sortme_alpha_plus(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (mylabel[c][i+1].getText() > mylabel[c][i+2].getText()) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

 	void sortme_num_min(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (to!int(mylabel[c][i+1].getText()) < to!int(mylabel[c][i+2].getText())) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}

	void sortme_num_plus(int c){
		foreach_reverse (n; 0..num_table_rows+1){
			bool swapped;
			foreach (i; 0 .. n){
				if (to!int(mylabel[c][i+1].getText()) > to!int(mylabel[c][i+2].getText())) {
					foreach (k ; 0..number_of_columns){
						string x=mylabel[k][i+1].getText();
						string y=mylabel[k][i+2].getText();
						mylabel[k][i+1].setText(y);
						mylabel[k][i+2].setText(x);
					}
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}


    void size_sort(){
		if (sort_order==true) sort_order=false; else sort_order=true;
		if (sort_order==true) sortme_num_plus(4); else sortme_num_min(4);
	}


    void name_sort(){
		statusbar.push(contextID, "Start sort by name");
		if (sort_order==true) sort_order=false; else sort_order=true;
		if (sort_order==true) sortme_alpha_plus(0); else sortme_alpha_min(0);
		statusbar.push(contextID, "Sort by name done");
			}

    void modification_sort(){
		statusbar.push(contextID, "Start sort by name");
		if (sort_order==true) sort_order=false; else sort_order=true;
		if (sort_order==true) sortme_alpha_plus(2); else sortme_alpha_min(2);
		statusbar.push(contextID, "Sort by name done");
			}

    void access_sort(){
		statusbar.push(contextID, "Start sort by name");
		if (sort_order==true) sort_order=false; else sort_order=true;
		if (sort_order==true) sortme_alpha_plus(3); else sortme_alpha_min(3);
		statusbar.push(contextID, "Sort by name done");
			}

    void extension_sort(){
		statusbar.push(contextID, "Start sort by name");
		if (sort_order==true) sort_order=false; else sort_order=true;
		if (sort_order==true) sortme_alpha_plus(5); else sortme_alpha_min(5);
		statusbar.push(contextID, "Sort by name done");
			}

	void myaddgrid(){
		//Uses maxrows
		int h=20;
		foreach( x ; 0..number_of_columns){
		    string labelText = format("Sort %s", columns[x]);
		    mybut[x][0] = new Button(labelText);
		    mybut[x][0].setSizeRequest(columnsize[x],h);
		    inner.attach(mybut[x][0], x, 0,1,1);	
			}
		mybut[0][0].addOnClicked(delegate void(Button b){name_sort();});
		mybut[2][0].addOnClicked(delegate void(Button b){modification_sort();});
		mybut[3][0].addOnClicked(delegate void(Button b){access_sort();});
		mybut[4][0].addOnClicked(delegate void(Button b){size_sort();});
		mybut[5][0].addOnClicked(delegate void(Button b){extension_sort();});
		
		foreach( y ; 1..maxrows){
			foreach( x ; 0..number_of_columns){
				if(x>3)
					mylabel[x][y] = new MyRLabel("",columnsize[x],h);
				else
					mylabel[x][y] = new MyLLabel("",columnsize[x],h);
				inner.attach(mylabel[x][y],x,y,1,1);	
				mybut[x][y] = new Button("");
				mybut[x][y].setSizeRequest(columnsize[x],h);
				privdatax[x]=x;
				privdatay[y]=y;
				mybut[x][y].setData("x",&privdatax[x]);
				mybut[x][y].setData("y",&privdatay[y]);
				mybut[x][y].addOnButtonRelease(&gridbuttonreleased);
				inner.attach(mybut[x][y],x,y,1,1);	
			}
		}
 	}
 	
	void show_handle(Connection conn){
		// Uses maxrows
		inner.add(new Label("test"));
		inner.queueDraw();
		string[] columns=["Name","C","Modification","Access","Size","Extension"];
		string sql=format(" SELECT * from %s ;",db_table);
		auto answer = conn.exec(sql);
		num_table_rows=-1;
		foreach(rownumber; answer.length.iota){
			auto arow=answer[rownumber];
			string name=arow[0].as!PGtext;
			string regfiltertext="\\b"~afilter.getText()~"\\b";
			auto reg = regex(regfiltertext);
			auto result = name.strip("_").matchAll(reg);
			int t=0;
			foreach (c; result)
				t=t+1;
			if (t>0){
				num_table_rows=num_table_rows+1;
				foreach(int xpos; 0..number_of_columns){
					mylabel[xpos][num_table_rows+1].setLabel(arow[xpos].as!PGtext);
				}
			if( num_table_rows + 2 > maxrows - 1 )
				break;
			}
		}
	}	
		
	void file_do(string name) {
		SysTime fileAccessTime, fileModificationTime;
		getTimes(name, fileAccessTime, fileModificationTime);
		string creation="XXX";
		string modification=fileModificationTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		if(modification[modification.length-1]=='.') modification=modification.chop();
		string access=fileAccessTime.toISOExtString().chop().chop().chop().chop().chop().chop();
		if(access[access.length-1]=='.') access=access.chop();
		string size=to!string(name.getSize());
		string myextension=extension(name).drop(1);
		//TODO
		string sql=format("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s');",db_table,name,creation,modification,access,size,myextension);
		auto answer = conn.exec(sql);
	}
	
	
	void cleantable(){
		auto answer = conn.exec(format(" TRUNCATE %s ;",db_table));
	}
	
	
	void filldb_handle(){
		statusbar.push(contextID, "Ask filling database");
		statusbar.queueDraw();
		vbox.queueDraw();
		if(sensitive==true){
			statusbar.push(contextID, "Start filling database");
			sensitive=false;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			string mydir=adir.getText();
			cleantable();

			auto dFiles = dirEntries(mydir, SpanMode.depth);
			foreach (d; dFiles){
				if (! d.isDir())
					if (d.isFile()) 
						{file_do(d.name);}
			}
			sensitive=true;
			fillbutton.setSensitive(sensitive);
			showbutton.setSensitive(sensitive);
			statusbar.push(contextID, "Filling database done");
		}
	}
	
	this(Connection myconn,int mymaxrows,string mydb_table,int sizex,int sizey,string[string] myapp_dict){
		conn=myconn;
		maxrows=mymaxrows;
		db_table=mydb_table;
		app_dict=myapp_dict;
		cleantable();
		//app_dict["conf"]="/usr/local/bin/leafpad";
		super("My Locate D-lang App");
		setSizeRequest(sizex,sizey);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);

			Box hboxdir=new Box(Orientation.HORIZONTAL,5);
			hboxdir.packStart((new Label("Enter Dir")),0,0,0);
				adir=new Entry("/home/x/Src");
				hboxdir.packStart(adir,1,1,0);
			vbox.packStart(hboxdir,0,0,0);

			fillbutton=new Button("Fill");
			fillbutton.addOnClicked(delegate void(Button b){filldb_handle();});
			vbox.packStart(fillbutton,0,0,0);

			Box hboxfilter=new Box(Orientation.HORIZONTAL,5);
			hboxfilter.packStart((new Label("Enter Filter")),0,0,0);
				afilter=new Entry("*conf$");
				hboxfilter.packStart(afilter,1,1,0);
			vbox.packStart(hboxfilter,0,0,0);

			showbutton=new Button("Show");
			showbutton.addOnClicked(delegate void(Button b){show_handle(conn);});
			showbutton.setSensitive(sensitive);
			vbox.packStart(showbutton,0,0,0);

			ScrolledWindow s=new ScrolledWindow();
				inner=new Grid();
 				myaddgrid();
				s.add(inner);
			vbox.packStart(s,1,1,1);

			statusbar=new Statusbar();
			uint contextID=statusbar.getContextId("My Description");
			statusbar.push(contextID, "--------");
			vbox.packStart(statusbar,0,0,0);
		
		add(vbox);
		showAll();
	}
}

Connection opendatabase(string host,string port,string db,string user,string pass){
    string url=format("hostaddr='%s' port='%s' dbname='%s' user='%s' password='%s'",host,port,db,user,pass);
	Connection conn = new Connection(url);
	auto answer = conn.exec("SELECT version()");
	return conn;
	}

int main(string[] args){
	string host="127.0.0.1";
	string port="5432";
	string db="x_db";
	string user="x";
	string pass="xxxxxxxx";
	string table=" files" ;
    string[string] app_dict=["conf" : "/usr/local/bin/leafpad"];

	int sizex=1000;
	int sizey=600;
	int maxrows=355;
	Main.init(args);
	Connection conn=opendatabase(host,port,db,user,pass);
	MyWindow window=new MyWindow(conn,maxrows,table,sizex,sizey,app_dict);
	Main.run();
	return 0;
}
